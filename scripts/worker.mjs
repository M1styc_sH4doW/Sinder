/**
 * Worker code for the main page in Sinder
 *  This will make the application faster
 */

"use strict";

import { digestMessage, loadUserData } from "./constants.mjs";
import sqlite3InitModule from "./SQLite/sqlite3.mjs";

const msgSender = "Worker:";
// Logic functions of the worker
let sqlite3;
let db;
let key;
let userData;
let profile;
let ivData;

const iniciate = async () => {
  sqlite3 = await sqlite3InitModule({
    print: console.log,
    printErr: console.error,
    printWarn: console.warn,
  });

  const capi = sqlite3.capi; /*C-style API*/ // ,oo = sqlite3.oo1; /*high-level OO API*/
  console.log(
    "sqlite3 version",
    capi.sqlite3_libversion(),
    capi.sqlite3_sourceid()
  );
};

const loadDB = async (file) => {
  try {
    if (!file) {
      console.error("Incorrect File");
      alert("Incorrect File, try again");
      return;
    } else {
      let read = new FileReader();
      read.readAsArrayBuffer(file); //console.log(read);
      read.onload = function () {
        let arrayBuffer = this.result;
        const p = sqlite3.wasm.allocFromTypedArray(arrayBuffer);
        db = new sqlite3.oo1.DB();
        const rc = sqlite3.capi.sqlite3_deserialize(
          db.pointer,
          "main",
          p,
          arrayBuffer.byteLength,
          arrayBuffer.byteLength,
          sqlite3.capi.SQLITE_DESERIALIZE_FREEONCLOSE
          // Optionally:
          // | sqlite3.capi.SQLITE_DESERIALIZE_RESIZEABLE
        );

        db.checkRc(rc);
      };
    }
  } catch (error) {
    throw error;
  }
};

const checkData = async (formData, keyBool) => {
  // Take the necesary elements
  let name = formData.get("name");
  let surname = formData.get("surname");
  let password = formData.get("password");

  try {
    if (keyBool) {
      // Create the necesary SHA
      let SHAName = await digestMessage(name);
      let ShaSurname = await digestMessage(surname);
      let SHAPassword = await digestMessage(password);
      // Rturn the id of the login person
      return await db.exec({
        sql: `SELECT user_id FROM sha_data 
          WHERE sha_name = '${SHAName}'
          AND sha_surname = '${ShaSurname}'
          AND sha_password = '${SHAPassword}'`,
        rowMode: "array",
        returnValue: "resultRows",
      });
    } else {
      // no cyphered data
      // Rturn the data of the login person
      return await db.exec({
        sql: `SELECT user_id FROM sha_data 
          WHERE sha_name = '${name}'
          AND sha_surname = '${surname}'
          AND sha_password = '${password}'`,
        rowMode: "array",
        returnValue: "resultRows",
      });
    }
  } catch (error) {
    throw error;
  }
};

const takeDataFromDB = async (id) => {
  try {
    return await db.exec({
      sql: `SELECT name, surname, age FROM users WHERE id = ${id} `,
      rowMode: "array",
      returnValue: "resultRows",
    });
  } catch (error) {
    throw error;
  }
};

const takeIV = async (id) => {
  try {
    return await db.exec({
      sql: `SELECT iv_name, iv_surname, iv_age FROM iv_session WHERE user_id = ${id} `,
      rowMode: "array",
      returnValue: "resultRows",
    });
  } catch (error) {
    throw error;
  }
};

self.onmessage = async (event) => {
  //console.log("Message received from main script");
  const method = event.data.method;
  const message = event.data.message;

  switch (method) {
    case "init":
      await iniciate();
      self.postMessage({ method: "iniciated", message: "Iniciated SQlite3" });
      break;

    case "loadDB":
      await loadDB(message);
      self.postMessage({ method: "readed", message: "Readed db" });
      break;

    case "loadKey":
      key = message; // Loaded from main thread
      console.log(`${msgSender} Key loaded succesfully`);
      break;

    case "login":
      if (key) {
        // cyphered db
        let id = await checkData(message, true);
        profile = await takeDataFromDB(id);
        ivData = await takeIV(id);
        await loadUserData(ivData, profile);
      } else {
        // We dont need here the SHA just normal query
        profile = await checkData(message, false);
      }
      break;

    default:
      console.warn(`${msgSender} No method from main thread found`);
      break;
  }
  //console.log("Returning to main script");
};
