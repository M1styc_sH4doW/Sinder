/* Sign In Sinder Module
    This module controls the main flow of the creation the mainline 
    databases or changes the existing database to update the dataset
    Here only has the main workflow, the external functions are in the 
    respective modules or javascript files.
*/

import {
  version,
  createBasicData,
  createKey,
  importKey,
  encryptMessage,
  insertIntoUsers,
  insertIntoLikings,
  insertIntoIV,
  digestMessage,
  insertIntoSHAData,
} from "./constants.mjs";

("use strict");

window.addEventListener("beforeunload", (event) => event.preventDefault());

let newDB = true;
let db;
let key;
let bool;
let counter = 0;
let byteArray;

const keyButton = document.getElementById("create-key");
const formSignin = document.getElementById("forma");
const keyReception = document.getElementById("insert-key");
const counterElm = document.getElementById("counter-users");
const salve = document.getElementById("save-database");
const contBtn = document.getElementById("next-page");
const dialog = document.querySelector("dialog");
const help = document.getElementById("help");
const closeBtn = document.querySelector("dialog button");
const ndata = document.getElementById("nerd-data");
const next = document.getElementById("next-page");

next.addEventListener("click", () => {
  location.href = "/main";
});

help.addEventListener("click", () => {
  dialog.showModal();
});

closeBtn.addEventListener("click", (event) => {
  event.preventDefault();
  dialog.close();
});

salve.disabled = true;

keyButton.addEventListener("click", createKey);

window.onload = function () {
  keyReception.value = null;
};

keyReception.addEventListener("change", async () => {
  let input = keyReception.files[0];
  await new Response(input)
    .json()
    .then((json) => importKey(json))
    .then((res) => (key = res))
    .catch(function (error) {
      console.error("Error:", error);
      alert(
        "There were an error trying to import the key. Please try again or reload the page"
      );
    });
});

console.log("Loading and initializing sqlite3 module...");
self
  .sqlite3InitModule({
    // We can redirect any stdout/stderr from the module
    // like so...
    print: console.log,
    printErr: console.error,
    printWarn: console.warn,
  })
  .then(async function (sqlite3) {
    // Initializing SQLite
    console.log("Done initializing. Running SQLite...");
    const capi = sqlite3.capi /*C-style API*/,
      oo = sqlite3.oo1; /*high-level OO API*/

    ndata.innerHTML = `Version: ${version} using Sqlite ${capi.sqlite3_libversion()}`;
    console.log(
      "sqlite3 version",
      capi.sqlite3_libversion(),
      capi.sqlite3_sourceid()
    );

    // Now we initialize the submit button of the form
    formSignin.addEventListener("submit", async (event) => {
      event.preventDefault();

      // Check if a key exists for secury
      if (!keyReception.value) {
        // No key
        bool = confirm("No key, continue?") && !keyReception.value; // MUST BE IN THIS ORDER
      } else {
        // Key avaiable
        bool = "key";
      }

      if (newDB) {
        db = new oo.DB("/sinderdb.sqlite3", "ct");
        /** Never(!) rely on garbage collection to clean up DBs and
         * (especially) prepared statements. Always wrap their lifetimes
         * in a try/finally construct, as demonstrated below. By and
         * large, client code can entirely avoid lifetime-related
         * complications of prepared statement objects by using the
         * DB.exec() method for SQL execution.
         * */
        // console.log("transient db =", db.filename);

        // Add closing logic
        contBtn.addEventListener("click", () => {
          db.close();
          location.href = "/main";
        });

        // Here we are going to unlock the Download database button,
        // because we have something to download
        salve.disabled = false;

        // Need to wait for the basic data to be created
        console.log("Creating data");
        await createBasicData(db);
        console.log("Data created");
        // Once created, we can take the data and update or create it
        // depending on the mode: encrypted of unencrypted.
        newDB = false;
      }

      /* MAIN MENU */
      switch (bool) {
        case true: // Continue without key
          console.log("Continue without key");
          await addUser(null, formSignin);
          break;
        case false: // No continue
          console.log("No continue");
          break;
        case "key": // Continue with key
          console.log("Continue with key");
          await addUser(key, formSignin);
          break;
        default:
          alert("Error processing, please try again");
          console.error("Bad processing, something went bad.");
          keyReception.value = null;
          break;
      }
    });
    salve.addEventListener("click", () => {
      byteArray = capi.sqlite3_js_db_export(db);
      exportDB(byteArray);
    });
  });

async function addUser(key, form) {
  const formData = new FormData(form);
  /* IMPORTANT:
    THIS FUNCTION ONLY CREATES DATA, NO UPDATES IT!
    This is meant to be in the Sign in Page to create new data into the database.
    In the main page of Sinder will be the function updateData to update existing data. 
  */
  let values = [];
  let middleSha = [];
  /* The order of the inputs are going to depend
  on how the HTML form is designed so if that changes,
  the order of the inputs must change
  */

  for (let index of formData) {
    if (!key) {
      // No key
      //values.push([index[0], index[1]]); // For debuging
      // No middle sha-348 necessary, because is all plain text
      values.push([null, index[1]]);
    } else {
      // Encrypt the messages
      await encryptMessage(index[1], key).then((res) => values.push(res));
      await digestMessage(index[1]).then((res) => middleSha.push(res));
    }
  }

  /*  // DEBUG
  console.log(middleSha);
  console.log(values); */

  await insertIntoUsers(
    db,
    values[0][1], // This inserts UInt8Array in the SQLite database. For take back the Arraybuffer: UInt8Array.buffer
    values[1][1], // surname
    values[2][1], // age
    values[3][1], // password
    values[4][1], // gender
    values[5][1], // orientation
    values[6][1], // relation
    values[7][1] // description
  );

  await insertIntoLikings(
    db,
    values[8][1],
    values[9][1],
    values[10][1], // smoking
    values[11][1],
    values[12][1],
    values[13][1], // fitness
    values[14][1],
    values[15][1],
    values[16][1], // sociability
    values[17][1],
    values[18][1],
    values[19][1], // personality
    values[20][1],
    values[21][1],
    values[22][1], // educaction
    values[23][1],
    values[24][1],
    values[25][1], //videogames
    values[26][1]
  );

  await insertIntoIV(
    db,
    values[0][0], //name
    values[1][0], // surname
    values[2][0], // age
    values[3][0], // password
    values[4][0], // gender
    values[5][0], // orientation
    values[6][0], // relation
    values[7][0], // description
    values[8][0],
    values[9][0],
    values[10][0], // smoking
    values[11][0],
    values[12][0],
    values[13][0], // fitness
    values[14][0],
    values[15][0],
    values[16][0], // sociability
    values[17][0],
    values[18][0],
    values[19][0], // personality
    values[20][0],
    values[21][0],
    values[22][0], // educaction
    values[23][0],
    values[24][0],
    values[25][0], //videogames
    values[26][0]
  );

  await insertIntoSHAData(
    db,
    middleSha[0], //name
    middleSha[1], // surname
    middleSha[2], // age
    middleSha[3], // password
    middleSha[4], // gender
    middleSha[5], // orientation
    middleSha[6], // relation
    middleSha[7], // description
    middleSha[8],
    middleSha[9],
    middleSha[10], // smoking
    middleSha[11],
    middleSha[12],
    middleSha[13], // fitness
    middleSha[14],
    middleSha[15],
    middleSha[16], // sociability
    middleSha[17],
    middleSha[18],
    middleSha[19], // personality
    middleSha[20],
    middleSha[21],
    middleSha[22], // educaction
    middleSha[23],
    middleSha[24],
    middleSha[25], //videogames
    middleSha[26]
  );

  counter++;
  counterElm.innerHTML = `<strong><p>You have added ${counter} new users!</p></strong>`;

  console.log("All done!");
  /*   // DEBUG
  db.exec({
    sql: "SELECT * FROM users",
    rowMode: "object",
    callback: function (row) {
      console.log(row);
    },
  });
  // DEBUG
  db.exec({
    sql: "SELECT * FROM likings",
    rowMode: "object",
    callback: function (row) {
      console.log(row);
    },
  });
  // DEBUG
  db.exec({
    sql: "SELECT * FROM sha_data",
    rowMode: "object",
    callback: function (row) {
      console.log(row);
    },
  }); */
}

function exportDB(byteArray) {
  console.log("click");
  if (confirm("Do you want to download the database to local storage?")) {
    const blob = new Blob([byteArray.buffer], {
      type: "application/x-sqlite3",
    });
    const a = document.createElement("a");
    document.body.appendChild(a);
    a.href = window.URL.createObjectURL(blob);
    a.download = db.filename.split("/").pop() || "my.sqlite3";
    a.addEventListener("click", function () {
      setTimeout(function () {
        console.log("Exported (possibly auto-downloaded) database");
        window.URL.revokeObjectURL(a.href);
        a.remove();
      }, 500);
    });
    a.click();
  } else {
    alert("Database still no saved, remember to save before living");
  }
}
