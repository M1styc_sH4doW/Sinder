-- Create a table named 'users'                    
-- This table will have the basic information of the profile
DROP TABLE IF EXISTS users;
CREATE TABLE users (
 id INTEGER PRIMARY KEY AUTOINCREMENT,
 creation_date BLOB NOT NULL,
 name BLOB NOT NULL DEFAULT 'name',
 surname BLOB NOT NULL DEFAULT 'surmame',
 age BLOB NOT NULL DEFAULT 0,
 password BLOB UNIQUE NOT NULL DEFAULT 'pas',
 gender BLOB NOT NULL DEFAULT 'gender',
 orientation BLOB NOT NULL DEFAULT 'orientation',
 relation BLOB NOT NULL DEFAULT 'rel',
 description BLOB NOT NULL DEFAULT 'description'
);
-- Now create the second table where extra information that will be given to the algorith
DROP TABLE IF EXISTS likings;
CREATE TABLE likings (
 user_id INTEGER PRIMARY KEY AUTOINCREMENT,
 age_range_min BLOB NOT NULL DEFAULT 25,
 age_range_max BLOB NOT NULL DEFAULT 30,
 smoking BLOB DEFAULT NULL,
 pets BLOB DEFAULT NULL,
 films BLOB DEFAULT NULL,
 fitness BLOB DEFAULT NULL,
 nightlife BLOB DEFAULT NULL,
 travel BLOB DEFAULT NULL,
 sociability BLOB DEFAULT NULL,
 work_position BLOB DEFAULT NULL,
 location BLOB DEFAULT NULL,
 personality BLOB DEFAULT NULL,
 zodiac BLOB DEFAULT NULL,
 music BLOB DEFAULT NULL,
 education BLOB DEFAULT NULL,
 reading BLOB DEFAULT NULL,
 coding BLOB DEFAULT NULL,
 videogames BLOB DEFAULT NULL,
 sport BLOB DEFAULT NULL, FOREIGN KEY (user_id) REFERENCES users(id)
);
-- I need to create statistics so i need a table for that
DROP TABLE IF EXISTS statistics;
CREATE TABLE statistics (
 user_id INTEGER PRIMARY KEY AUTOINCREMENT,
 session_time BLOB,
 yes_counter BLOB DEFAULT 0,
 no_counter BLOB DEFAULT 0,
 last_yes BLOB DEFAULT NULL,
 value_user BLOB NOT NULL DEFAULT 75,
 historic_people_passed BLOB DEFAULT NULL,
 affinity_porcentaje BLOB DEFAULT 0, FOREIGN KEY (user_id) REFERENCES users(id)
);
-- For better encryption, one table will have the data of the last iv
-- This will encrease the randomness on the keys to encrypt
DROP TABLE IF EXISTS iv_session;
CREATE TABLE iv_session (
 user_id INTEGER PRIMARY KEY AUTOINCREMENT,
 iv_name BLOB DEFAULT NULL,
 iv_surname BLOB DEFAULT NULL,
 iv_age BLOB DEFAULT NULL,
 iv_password BLOB DEFAULT NULL,
 iv_gender BLOB DEFAULT NULL,
 iv_orientation BLOB DEFAULT NULL,
 iv_relation BLOB DEFAULT NULL,
 iv_description BLOB DEFAULT NULL,
 iv_age_range_min BLOB DEFAULT NULL,
 iv_age_range_max BLOB DEFAULT NULL,
 iv_smoking BLOB DEFAULT NULL,
 iv_pets BLOB DEFAULT NULL,
 iv_films BLOB DEFAULT NULL,
 iv_fitness BLOB DEFAULT NULL,
 iv_nightlife BLOB DEFAULT NULL,
 iv_travel BLOB DEFAULT NULL,
 iv_sociability BLOB DEFAULT NULL,
 iv_work_position BLOB DEFAULT NULL,
 iv_location BLOB DEFAULT NULL,
 iv_personality BLOB DEFAULT NULL,
 iv_zodiac BLOB DEFAULT NULL,
 iv_music BLOB DEFAULT NULL,
 iv_education BLOB DEFAULT NULL,
 iv_reading BLOB DEFAULT NULL,
 iv_coding BLOB DEFAULT NULL,
 iv_videogames BLOB DEFAULT NULL,
 iv_sport BLOB DEFAULT NULL, FOREIGN KEY (user_id) REFERENCES users(id)
);
 
-- For the login, for privacy reasons I need to create a middle table where to store the SHA512 
-- version of the data to check if someone is logged and then, continue to the main database
-- only in case the database is cyphered, otherwise is unnecesary due to the fact that everything
-- is plain text. I know i don't need all the variables but for now, i'll keep them all.
DROP TABLE IF EXISTS sha_data;
CREATE TABLE sha_data (
user_id INTEGER PRIMARY KEY autoincrement,
sha_name TEXT DEFAULT NULL,
sha_surname TEXT DEFAULT NULL,
sha_age TEXT DEFAULT NULL,
sha_password TEXT DEFAULT NULL,
sha_gender TEXT DEFAULT NULL,
sha_orientation TEXT DEFAULT NULL,
sha_relation TEXT DEFAULT NULL,
sha_description TEXT DEFAULT NULL,
sha_age_range_min TEXT DEFAULT NULL,
sha_age_range_max TEXT DEFAULT NULL,
sha_smoking TEXT DEFAULT NULL,
sha_pets TEXT DEFAULT NULL,
sha_films TEXT DEFAULT NULL,
sha_fitness TEXT DEFAULT NULL,
sha_nightlife TEXT DEFAULT NULL,
sha_travel TEXT DEFAULT NULL,
sha_sociability TEXT DEFAULT NULL,
sha_work_position TEXT DEFAULT NULL,
sha_location TEXT DEFAULT NULL,
sha_personality TEXT DEFAULT NULL,
sha_zodiac TEXT DEFAULT NULL,
sha_music TEXT DEFAULT NULL,
sha_education TEXT DEFAULT NULL,
sha_reading TEXT DEFAULT NULL,
sha_coding TEXT DEFAULT NULL,
sha_videogames TEXT DEFAULT NULL,
sha_sport TEXT DEFAULT NULL, FOREIGN KEY (user_id) REFERENCES users(id)
);